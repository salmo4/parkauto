package parkingApp.logic;



import main.java.parkingApp.repository.RepositoryCar;
import parkingApp.exceptions.NoParkingSpaceAvailableException;
import parkingApp.exceptions.ParkingWasNotPaidException;
import parkingApp.model.Car;
import parkingApp.model.Parking;
import parkingApp.repository.RepositoryParking;

import java.util.concurrent.TimeUnit;

public class Logic {
    Parking parking = new Parking();
    RepositoryCar repositoryCar = new RepositoryCar();
    RepositoryParking repositoryParking = new RepositoryParking();

    public Logic() {
        parking.setPlacesAvailable(100);
    }

    public Integer getAvailableSpaces() {
        return parking.getPlacesAvailable();
    }

    public int enterIntoParking() throws NoParkingSpaceAvailableException {
        if (parking.getPlacesAvailable() > 0) {
            parking.decreaseAvailablePlaces();
            Integer carId = repositoryCar.addCar();
            Car carById = repositoryCar.findCarById(carId);
            repositoryParking.addCarIntoParking(new Parking(carById));
            return carId;//salveaza si returneaza
        } else {
            throw new NoParkingSpaceAvailableException("We are sorry, the parking is full!");
        }
    }

    public long getMinutes(int carId) {
        Car car = repositoryCar.findCarById(carId);
        long databaseTime = car.getDateIn();
        long difference = System.currentTimeMillis() - databaseTime;
        return TimeUnit.MILLISECONDS.toMinutes(difference);
    }

    public double amountToPay(long timeInMinutes) {
        double fee = 0.10;
        double amountToPay;
        if (timeInMinutes < 15) {
            amountToPay = 2;
        }else if (timeInMinutes<1440){
            amountToPay=300;
        }
        else
            amountToPay = timeInMinutes * fee;
        return amountToPay;
    }

    public boolean getOutOfParking(double amountToPay, double amountPaid, int carId) throws ParkingWasNotPaidException {
        if (amountToPay <= amountPaid) {
            repositoryParking.updateParking(carId, true, amountPaid);
            parking.increaseAvailablePlaces();
            repositoryCar.addDateOut(carId);
            return true;
        } else {
            throw new ParkingWasNotPaidException("The amount is not paid!");
        }
    }


}
