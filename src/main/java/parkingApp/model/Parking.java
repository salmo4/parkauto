package parkingApp.model;


import javax.persistence.*;


@Entity
@Table(name = "parking")
public class Parking {
    private int placesAvailable;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer parkingId;

    @Column(name = "payment_amount")
    private double paymentAmount;

    @Column(name = "paid")
    private boolean isPaid;

    @OneToOne
    @JoinColumn(name = "carId")
    private Car car;


    public Parking() {
    }

    public Parking(Car car) {
        this.car = car;
    }

    public void decreaseAvailablePlaces() {
        placesAvailable--;
    }

    public void increaseAvailablePlaces() {
        placesAvailable++;
    }

    public int getPlacesAvailable() {
        return placesAvailable;
    }

    public void setPlacesAvailable(int placesAvailable) {
        this.placesAvailable = placesAvailable;
    }

    public Integer getParkingId() {
        return parkingId;
    }

    public void setParkingId(Integer parkingId) {
        this.parkingId = parkingId;
    }

    public double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public boolean isPaid() {
        return isPaid;
    }

    public void setPaid(boolean paid) {
        isPaid = paid;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }
}
