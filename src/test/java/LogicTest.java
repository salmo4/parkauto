package test.java;

import main.java.parkingApp.repository.RepositoryCar;
import org.junit.Assert;
import org.junit.Test;
import parkingApp.exceptions.NoParkingSpaceAvailableException;
import parkingApp.exceptions.ParkingWasNotPaidException;
import parkingApp.logic.Logic;
import parkingApp.model.Parking;

import parkingApp.repository.RepositoryParking;

public class LogicTest {
    @Test(expected =NoParkingSpaceAvailableException.class)//test parcare plina
 public void testEnterIntoParking() throws NoParkingSpaceAvailableException {
     Parking parking = new Parking();
     RepositoryCar repositoryCar = new RepositoryCar();
     RepositoryParking repositoryParking = new RepositoryParking();
     Logic logic=new Logic();
     parking.setPlacesAvailable(0);
     logic.enterIntoParking();
    }
    @Test//verifica id ul
    public void idTestEnterIntoParking() throws NoParkingSpaceAvailableException {
        Parking parking = new Parking();
        RepositoryCar repositoryCar = new RepositoryCar();
        RepositoryParking repositoryParking = new RepositoryParking();
        Logic logic=new Logic();
        int id=logic.enterIntoParking();//asta astioneaza asupra bazai de date adica modofica in ea ceva sau doar testeaza?
        Assert.assertEquals(100-parking.getPlacesAvailable(),id);
    }
    //o metoda care sa fie id= cel mai mare numar de pe coloana cu indexul din baza de date??

    @Test(expected = ParkingWasNotPaidException.class)//nu a platit destul
    public void testGetOutOfParking() throws ParkingWasNotPaidException {
        Parking parking = new Parking();
        RepositoryCar repositoryCar = new RepositoryCar();
        RepositoryParking repositoryParking = new RepositoryParking();
        Logic logic=new Logic();
        logic.getOutOfParking(12,10,1);
    }

}
